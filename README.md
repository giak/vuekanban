# vuekanban

Kanban made with Vue.js

## Technical
- vue 2.5^
- vuex 3.0^
- vue-router 3.0^
- vue-persistedstate 2.5^ (vuew store in localstorage)
- vue-i18n 8.0^
- vue-markdown 2.2^ (render text)
- vue-prism 1.0^ (render code in markdown)
- vuedraggable 2.16^ (drag & drop)
- boostrap-vue + bootswatch (boostrap 4^ + theme)
- fortawesome 5^ (svg icons)

## Features
Full CRUD for lanes and Items
- Lanes + color
- Items + projects + priorities
- I18n
- Draggable

All datas are saved in a global JSON.
```json
{
  "logErrors":null,
  "kanbanItems":{
    "nextId":8
  },
  "kanbanLanes":{
    "nextLaneId":9
  },
  "kanbanLanesItems":{
    "lanesItems":[
      {
        "id":7,
        "name":"1111111111111",
        "color":null,
        "state":1,
        "items":[
          {
            "id":6,
            "title":"sdddddddddddd",
            "text":null,
            "todoList":null,
            "state":null,
            "done":null,
            "date":"2018-08-17",
            "priority":null,
            "project":null
          },
        ]
      },
    ]
  },
}
```