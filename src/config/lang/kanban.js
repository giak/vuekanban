const kanban = {
  fr: {
    kanban: {
      lane: {
        opened: 'fermé',
        closed: 'ouvert',
        newLane: 'nouvelle lane',
        save: 'sauvegarder',
        delete: "Effacer",
        cancel: "Annuler",
        areYouSureToDelete: "Etes-vous sur de vouloir effacer ?",
        DeleteLane: "Effacement d'une Lane",
        fieldIsRequired: "A remplir",
        textTooSmall: "La longueur du texte n\'est pas suffisante. [ actuellement : {0} ; minimum requis : {1} ]",
        previewColors: "Prévisualisation des couleurs",
        previewColorSelected: "Prévisualisation de la couleur sélectionnée",
        input: {
          label: 'Lane',
          placeholder: 'la lane'
        },
        select: {
          colors: "Les couleurs", // TODO clean this mess
          placeholder: 'Liste des lanes (OBLIGATOIRE)',
          error: 'Lane est obligatoire.'
        }
      },
      project: {
        select: {
          placeholder: 'Liste des projets'
        }
      },
      priority: {
        select: {
          placeholder: 'Liste des priorités'
        }
      },
      item: {
        done : 'Fait',
        todo : 'a faire',
        task : 'tache | taches',
        input: {
          placeholder: 'Titre',
          error: 'Le titre est obligatoire.',
          errorLength: 'Il faut un minimum de {0} caractères'
        },
        date: {
          placeholder: 'Date'
        },
        textareaContent: {
          placeholder: 'Le contenu ...'
        },
        textareaTodos: {
          placeholder: 'Todos / Remarques'
        },
        submit: {
          OK: 'Sauvegarde effectué',
          ERROR: 'Merci de bien remplir le formulaire.',
          PENDING: 'en cours ...',
          new: 'nouvelle tache',
          save: 'sauvegarder'
        }
      }
    }
  }
};

export default kanban;
