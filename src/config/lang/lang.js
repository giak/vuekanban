import Vue     from 'vue';
import VueI18n from 'vue-i18n';

Vue.use(VueI18n);

import kanban from './kanban';


export const i18n = new VueI18n({
                                  locale: 'fr',
                                  messages: kanban
                                });