export default {

  tabProject: [
    {id: 'CRM', label: "CRM"},
    {id: 'CRC', label: "CRC"},
    {id: 'LOC', label: "LOC"}
  ],

  tabPriorities: [
    {id: '0', backGroundColor: "#8c8c85", color: "", label: "P 0"},
    {id: '1', backGroundColor: "#444444", color: "", label: "P 1"},
    {id: '2', backGroundColor: "#88d9eb", color: "", label: "P 2"},
    {id: '3', backGroundColor: "#f5c400", color: "", label: "P 3"},
    {id: '4', backGroundColor: "#ff9900", color: "", label: "P 4"},
    {id: '5', backGroundColor: "#c70000", color: "", label: "P 5"}
  ],


  tabColors: [
    {
      id: "#EFDECD",
      name: "Almond",
      rgb: "(239, 222, 205)"
    },
    {
      id: "#CD9575",
      name: "Antique Brass",
      rgb: "(205, 149, 117)"
    },
    {
      id: "#FDD9B5",
      name: "Apricot",
      rgb: "(253, 217, 181)"
    },
    {
      id: "#78DBE2",
      name: "Aquamarine",
      rgb: "(120, 219, 226)"
    },
    {
      id: "#87A96B",
      name: "Asparagus",
      rgb: "(135, 169, 107)"
    },
    {
      id: "#FFA474",
      name: "Atomic Tangerine",
      rgb: "(255, 164, 116)"
    },
    {
      id: "#FAE7B5",
      name: "Banana Mania",
      rgb: "(250, 231, 181)"
    },
    {
      id: "#9F8170",
      name: "Beaver",
      rgb: "(159, 129, 112)"
    },
    {
      id: "#FD7C6E",
      name: "Bittersweet",
      rgb: "(253, 124, 110)"
    },
    {
      id: "#000000",
      name: "Black",
      rgb: "(0,0,0)"
    },
    {
      id: "#ACE5EE",
      name: "Blizzard Blue",
      rgb: "(172, 229, 238)"
    },
    {
      id: "#1F75FE",
      name: "Blue",
      rgb: "(31, 117, 254)"
    },
    {
      id: "#A2A2D0",
      name: "Blue Bell",
      rgb: "(162, 162, 208)"
    },
    {
      id: "#6699CC",
      name: "Blue Gray",
      rgb: "(102, 153, 204)"
    },
    {
      id: "#0D98BA",
      name: "Blue Green",
      rgb: "(13, 152, 186)"
    },
    {
      id: "#7366BD",
      name: "Blue Violet",
      rgb: "(115, 102, 189)"
    },
    {
      id: "#DE5D83",
      name: "Blush",
      rgb: "(222, 93, 131)"
    },
    {
      id: "#CB4154",
      name: "Brick Red",
      rgb: "(203, 65, 84)"
    },
    {
      id: "#B4674D",
      name: "Brown",
      rgb: "(180, 103, 77)"
    },
    {
      id: "#FF7F49",
      name: "Burnt Orange",
      rgb: "(255, 127, 73)"
    },
    {
      id: "#EA7E5D",
      name: "Burnt Sienna",
      rgb: "(234, 126, 93)"
    },
    {
      id: "#B0B7C6",
      name: "Cadet Blue",
      rgb: "(176, 183, 198)"
    },
    {
      id: "#FFFF99",
      name: "Canary",
      rgb: "(255, 255, 153)"
    },
    {
      id: "#1CD3A2",
      name: "Caribbean Green",
      rgb: "(28, 211, 162)"
    },
    {
      id: "#FFAACC",
      name: "Carnation Pink",
      rgb: "(255, 170, 204)"
    },
    {
      id: "#DD4492",
      name: "Cerise",
      rgb: "(221, 68, 146)"
    },
    {
      id: "#1DACD6",
      name: "Cerulean",
      rgb: "(29, 172, 214)"
    },
    {
      id: "#BC5D58",
      name: "Chestnut",
      rgb: "(188, 93, 88)"
    },
    {
      id: "#DD9475",
      name: "Copper",
      rgb: "(221, 148, 117)"
    },
    {
      id: "#9ACEEB",
      name: "Cornflower",
      rgb: "(154, 206, 235)"
    },
    {
      id: "#FFBCD9",
      name: "Cotton Candy",
      rgb: "(255, 188, 217)"
    },
    {
      id: "#FDDB6D",
      name: "Dandelion",
      rgb: "(253, 219, 109)"
    },
    {
      id: "#2B6CC4",
      name: "Denim",
      rgb: "(43, 108, 196)"
    },
    {
      id: "#EFCDB8",
      name: "Desert Sand",
      rgb: "(239, 205, 184)"
    },
    {
      id: "#6E5160",
      name: "Eggplant",
      rgb: "(110, 81, 96)"
    },
    {
      id: "#CEFF1D",
      name: "Electric Lime",
      rgb: "(206, 255, 29)"
    },
    {
      id: "#71BC78",
      name: "Fern",
      rgb: "(113, 188, 120)"
    },
    {
      id: "#6DAE81",
      name: "Forest Green",
      rgb: "(109, 174, 129)"
    },
    {
      id: "#C364C5",
      name: "Fuchsia",
      rgb: "(195, 100, 197)"
    },
    {
      id: "#CC6666",
      name: "Fuzzy Wuzzy",
      rgb: "(204, 102, 102)"
    },
    {
      id: "#E7C697",
      name: "Gold",
      rgb: "(231, 198, 151)"
    },
    {
      id: "#FCD975",
      name: "Goldenrod",
      rgb: "(252, 217, 117)"
    },
    {
      id: "#A8E4A0",
      name: "Granny Smith Apple",
      rgb: "(168, 228, 160)"
    },
    {
      id: "#95918C",
      name: "Gray",
      rgb: "(149, 145, 140)"
    },
    {
      id: "#1CAC78",
      name: "Green",
      rgb: "(28, 172, 120)"
    },
    {
      id: "#1164B4",
      name: "Green Blue",
      rgb: "(17, 100, 180)"
    },
    {
      id: "#F0E891",
      name: "Green Yellow",
      rgb: "(240, 232, 145)"
    },
    {
      id: "#FF1DCE",
      name: "Hot Magenta",
      rgb: "(255, 29, 206)"
    },
    {
      id: "#B2EC5D",
      name: "Inchworm",
      rgb: "(178, 236, 93)"
    },
    {
      id: "#5D76CB",
      name: "Indigo",
      rgb: "(93, 118, 203)"
    },
    {
      id: "#CA3767",
      name: "Jazzberry Jam",
      rgb: "(202, 55, 103)"
    },
    {
      id: "#3BB08F",
      name: "Jungle Green",
      rgb: "(59, 176, 143)"
    },
    {
      id: "#FEFE22",
      name: "Laser Lemon",
      rgb: "(254, 254, 34)"
    },
    {
      id: "#FCB4D5",
      name: "Lavender",
      rgb: "(252, 180, 213)"
    },
    {
      id: "#FFF44F",
      name: "Lemon Yellow",
      rgb: "(255, 244, 79)"
    },
    {
      id: "#FFBD88",
      name: "Macaroni and Cheese",
      rgb: "(255, 189, 136)"
    },
    {
      id: "#F664AF",
      name: "Magenta",
      rgb: "(246, 100, 175)"
    },
    {
      id: "#AAF0D1",
      name: "Magic Mint",
      rgb: "(170, 240, 209)"
    },
    {
      id: "#CD4A4C",
      name: "Mahogany",
      rgb: "(205, 74, 76)"
    },
    {
      id: "#EDD19C",
      name: "Maize",
      rgb: "(237, 209, 156)"
    },
    {
      id: "#979AAA",
      name: "Manatee",
      rgb: "(151, 154, 170)"
    },
    {
      id: "#FF8243",
      name: "Mango Tango",
      rgb: "(255, 130, 67)"
    },
    {
      id: "#C8385A",
      name: "Maroon",
      rgb: "(200, 56, 90)"
    },
    {
      id: "#EF98AA",
      name: "Mauvelous",
      rgb: "(239, 152, 170)"
    },
    {
      id: "#FDBCB4",
      name: "Melon",
      rgb: "(253, 188, 180)"
    },
    {
      id: "#1A4876",
      name: "Midnight Blue",
      rgb: "(26, 72, 118)"
    },
    {
      id: "#30BA8F",
      name: "Mountain Meadow",
      rgb: "(48, 186, 143)"
    },
    {
      id: "#C54B8C",
      name: "Mulberry",
      rgb: "(197, 75, 140)"
    },
    {
      id: "#1974D2",
      name: "Navy Blue",
      rgb: "(25, 116, 210)"
    },
    {
      id: "#FFA343",
      name: "Neon Carrot",
      rgb: "(255, 163, 67)"
    },
    {
      id: "#BAB86C",
      name: "Olive Green",
      rgb: "(186, 184, 108)"
    },
    {
      id: "#FF7538",
      name: "Orange",
      rgb: "(255, 117, 56)"
    },
    {
      id: "#FF2B2B",
      name: "Orange Red",
      rgb: "(255, 43, 43)"
    },
    {
      id: "#F8D568",
      name: "Orange Yellow",
      rgb: "(248, 213, 104)"
    },
    {
      id: "#E6A8D7",
      name: "Orchid",
      rgb: "(230, 168, 215)"
    },
    {
      id: "#414A4C",
      name: "Outer Space",
      rgb: "(65, 74, 76)"
    },
    {
      id: "#FF6E4A",
      name: "Outrageous Orange",
      rgb: "(255, 110, 74)"
    },
    {
      id: "#1CA9C9",
      name: "Pacific Blue",
      rgb: "(28, 169, 201)"
    },
    {
      id: "#FFCFAB",
      name: "Peach",
      rgb: "(255, 207, 171)"
    },
    {
      id: "#C5D0E6",
      name: "Periwinkle",
      rgb: "(197, 208, 230)"
    },
    {
      id: "#FDDDE6",
      name: "Piggy Pink",
      rgb: "(253, 221, 230)"
    },
    {
      id: "#158078",
      name: "Pine Green",
      rgb: "(21, 128, 120)"
    },
    {
      id: "#FC74FD",
      name: "Pink Flamingo",
      rgb: "(252, 116, 253)"
    },
    {
      id: "#F78FA7",
      name: "Pink Sherbet",
      rgb: "(247, 143, 167)"
    },
    {
      id: "#8E4585",
      name: "Plum",
      rgb: "(142, 69, 133)"
    },
    {
      id: "#7442C8",
      name: "Purple Heart",
      rgb: "(116, 66, 200)"
    },
    {
      id: "#9D81BA",
      name: "Purple Mountain's Majesty",
      rgb: "(157, 129, 186)"
    },
    {
      id: "#FE4EDA",
      name: "Purple Pizzazz",
      rgb: "(254, 78, 218)"
    },
    {
      id: "#FF496C",
      name: "Radical Red",
      rgb: "(255, 73, 108)"
    },
    {
      id: "#D68A59",
      name: "Raw Sienna",
      rgb: "(214, 138, 89)"
    },
    {
      id: "#714B23",
      name: "Raw Umber",
      rgb: "(113, 75, 35)"
    },
    {
      id: "#FF48D0",
      name: "Razzle Dazzle Rose",
      rgb: "(255, 72, 208)"
    },
    {
      id: "#E3256B",
      name: "Razzmatazz",
      rgb: "(227, 37, 107)"
    },
    {
      id: "#EE204D",
      name: "Red",
      rgb: "(238,32 ,77 )"
    },
    {
      id: "#FF5349",
      name: "Red Orange",
      rgb: "(255, 83, 73)"
    },
    {
      id: "#C0448F",
      name: "Red Violet",
      rgb: "(192, 68, 143)"
    },
    {
      id: "#1FCECB",
      name: "Robin's Egg Blue",
      rgb: "(31, 206, 203)"
    },
    {
      id: "#7851A9",
      name: "Royal Purple",
      rgb: "(120, 81, 169)"
    },
    {
      id: "#FF9BAA",
      name: "Salmon",
      rgb: "(255, 155, 170)"
    },
    {
      id: "#FC2847",
      name: "Scarlet",
      rgb: "(252, 40, 71)"
    },
    {
      id: "#76FF7A",
      name: "Screamin' Green",
      rgb: "(118, 255, 122)"
    },
    {
      id: "#9FE2BF",
      name: "Sea Green",
      rgb: "(159, 226, 191)"
    },
    {
      id: "#A5694F",
      name: "Sepia",
      rgb: "(165, 105, 79)"
    },
    {
      id: "#8A795D",
      name: "Shadow",
      rgb: "(138, 121, 93)"
    },
    {
      id: "#45CEA2",
      name: "Shamrock",
      rgb: "(69, 206, 162)"
    },
    {
      id: "#FB7EFD",
      name: "Shocking Pink",
      rgb: "(251, 126, 253)"
    },
    {
      id: "#CDC5C2",
      name: "Silver",
      rgb: "(205, 197, 194)"
    },
    {
      id: "#80DAEB",
      name: "Sky Blue",
      rgb: "(128, 218, 235)"
    },
    {
      id: "#ECEABE",
      name: "Spring Green",
      rgb: "(236, 234, 190)"
    },
    {
      id: "#FFCF48",
      name: "Sunglow",
      rgb: "(255, 207, 72)"
    },
    {
      id: "#FD5E53",
      name: "Sunset Orange",
      rgb: "(253, 94, 83)"
    },
    {
      id: "#FAA76C",
      name: "Tan",
      rgb: "(250, 167, 108)"
    },
    {
      id: "#18A7B5",
      name: "Teal Blue",
      rgb: "(24, 167, 181)"
    },
    {
      id: "#EBC7DF",
      name: "Thistle",
      rgb: "(235, 199, 223)"
    },
    {
      id: "#FC89AC",
      name: "Tickle Me Pink",
      rgb: "(252, 137, 172)"
    },
    {
      id: "#DBD7D2",
      name: "Timberwolf",
      rgb: "(219, 215, 210)"
    },
    {
      id: "#17806D",
      name: "Tropical Rain Forest",
      rgb: "(23, 128, 109)"
    },
    {
      id: "#DEAA88",
      name: "Tumbleweed",
      rgb: "(222, 170, 136)"
    },
    {
      id: "#77DDE7",
      name: "Turquoise Blue",
      rgb: "(119, 221, 231)"
    },
    {
      id: "#FFFF66",
      name: "Unmellow Yellow",
      rgb: "(255, 255, 102)"
    },
    {
      id: "#926EAE",
      name: "Violet (Purple)",
      rgb: "(146, 110, 174)"
    },
    {
      id: "#324AB2",
      name: "Violet Blue",
      rgb: "(50, 74, 178)"
    },
    {
      id: "#F75394",
      name: "Violet Red",
      rgb: "(247, 83, 148)"
    },
    {
      id: "#FFA089",
      name: "Vivid Tangerine",
      rgb: "(255, 160, 137)"
    },
    {
      id: "#8F509D",
      name: "Vivid Violet",
      rgb: "(143, 80, 157)"
    },
    {
      id: "#FFFFFF",
      name: "White",
      rgb: "(255, 255, 255)"
    },
    {
      id: "#A2ADD0",
      name: "Wild Blue Yonder",
      rgb: "(162, 173, 208)"
    },
    {
      id: "#FF43A4",
      name: "Wild Strawberry",
      rgb: "(255, 67, 164)"
    },
    {
      id: "#FC6C85",
      name: "Wild Watermelon",
      rgb: "(252, 108, 133)"
    },
    {
      id: "#CDA4DE",
      name: "Wisteria",
      rgb: "(205, 164, 222)"
    },
    {
      id: "#FCE883",
      name: "Yellow",
      rgb: "(252, 232, 131)"
    },
    {
      id: "#C5E384",
      name: "Yellow Green",
      rgb: "(197, 227, 132)"
    },
    {
      id: "#FFAE42",
      name: "Yellow Orange",
      rgb: "(255, 174, 66)"
    }
  ]


};