export default {
  LOG_ERRORS_ACTION: function ({commit}, errors) {
    commit('LOG_ERRORS', errors);
  }
};
