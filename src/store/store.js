import Vue                  from 'vue';
import Vuex                 from 'vuex';
import createPersistedState from "vuex-persistedstate";
import actions              from './actions';
import getters              from './getters';
import mutations            from './mutations';


import kanbanItems      from './modules/kanbanItems.js';
import kanbanLanes      from './modules/kanbanLanes';
import kanbanLanesItems from './modules/kanbanLanesItems';

Vue.use(Vuex);

const state = {
  logErrors: null // Vue.config.errorHandler
};


export const store = new Vuex.Store({
  state,
  getters,
  mutations,
  actions,
  modules: {
    kanbanItems,
    kanbanLanes,
    kanbanLanesItems
  },
  plugins: [createPersistedState({key: 'kanbanStore'})]
});

