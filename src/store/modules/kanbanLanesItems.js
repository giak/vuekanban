import {setItem, getItemIndex, getParentItemIndex, setParentItemIndex} from "../../tools/utils/itemLaneIntegrator";


const state = {
  lanesItems: [] // lanes + items
};

const getters = {};

const mutations = {
  deleteLanesItems(state, index) {
    delete state.lanesItems[index].items;
  },


  deleteLanesItemsAll(state) {
    state.lanesItems = [];
  },
  /**
   *
   * @param state
   * @param item
   *        item.newItemIndex   index de l'array
   *        item.newItem        data
   * @constructor
   */
  ADD_LANES_ITEMS(state, item) {
    // console.log('ADD_LANES_ITEMS :: ', JSON.stringify(item, null, 2));
    if (item.newItemIndex === null) {
      // console.log(' :: ', JSON.stringify('push',null,2))
      state.lanesItems.push(item.newItem);
    } else {
      // console.log(' :: ', JSON.stringify('add indexed',null,2))
      state.lanesItems[item.newItemIndex] = item.newItem;
    }
  },
  UPDATE_LANES_ITEMS(state, lanesItems) {
    state.lanesItems = lanesItems;
  }
};

const actions = {
  DELETE_LANES_ITEMS_ACTION: function ({commit, state, dispatch}) {
    for (let [index, el] of state.lanesItems.entries()) {
      commit('deleteLanesItems', index);
    }
  },

  UPDATE_LANES_ITEMS_ACTION: function ({commit, state, dispatch}, item) {
    // let parentItemIndex = getParentItemIndex(state.lanesItems, item);
    // console.log('parentItemIndex :: ', JSON.stringify(parentItemIndex, null, 2));
    // console.log('parentIndex :: ', JSON.stringify(state.lanes[parentItemIndex.parentIndex], null, 2));

    let lanesItems = setParentItemIndex(state.lanesItems, item);
    // console.log('lanesItems :: ', JSON.stringify(lanesItems, null, 2));
    commit('UPDATE_LANES_ITEMS', lanesItems);
  },
  /**
   *
   * @param commit
   * @param item     @see ADD_LANES_ITEMS
   * @constructor
   */
  ADD_LANES_ITEMS_ACTION: function ({commit}, item) {
    // console.log('ADD_LANES_ITEMS_ACTION :: ', JSON.stringify(item,null,2))
    commit('ADD_LANES_ITEMS', item);
  },


  DRAG__LANES_ITEMS_ACTION: function ({commit}, item) {

  }
};

export default {
  namespaced: true,
  state,
  mutations,
  actions,
  getters
};