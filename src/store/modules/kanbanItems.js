import {setItem, getItemIndex, getParentItemIndex, setParentItemIndex} from "../../tools/utils/itemLaneIntegrator";

const state = {
  items: [], // items seul
  nextId: 1,
};

const getters = {};

const mutations = {
  deleteItems(state) {
    state.items = [];
  },

  updateDragItems(state, items) {
    state.items = items;
  },
  DRAG_ITEM(state, item, idLane) {
    // console.log('DRAG_ITEM idLane :: ', JSON.stringify(idLane, null, 2));
    // console.log('DRAG_ITEM item :: ', JSON.stringify(item, null, 2));
    for (let [index, el] of item.value.entries()) {
      // state.items[index].lane = idLane
      console.log('DRAG_ITEM el :: ', JSON.stringify(el, null, 2));
    }
  },
  UPDATE_ID: function (state) {
    state.nextId += 1;
  },
  ADD_ITEM: function (state, payload) {
    state.items.push(payload);
  },
  UPDATE_ITEM: function (state, payload, index) {
    state.items[index] = payload;
  },
  DELETE_ITEM: function (state, index) {
    state.items.splice(index, 1);
  }
};

const actions = {
  /**
   * @return {null}
   */
  ADD_ITEM_ACTION: function ({commit, state, dispatch}, item) {
    let newItem = setItem(item, state.nextId);
    console.log(' :: ', JSON.stringify(newItem,null,2))
    if (newItem === null) return null;

    if (item.id && item.id > 0) {
      state.items.forEach(function (element, index) {
        if (element.id === item.id) {
          commit('UPDATE_ITEM', newItem, index);
        }
      });
    } else {
      commit('ADD_ITEM', newItem);
      commit('UPDATE_ID');
    }

    // push to lanes
    dispatch('kanbanLanesItems/UPDATE_LANES_ITEMS_ACTION', newItem, {root: true});
  },
  DELETE_ITEM_ACTION: function ({commit, state}, item) {
    let index = getItemIndex(state.items, item.id);
    commit('DELETE_ITEM', index);
  }
};

export default {
  namespaced: true,
  state,
  mutations,
  actions,
  getters
};

