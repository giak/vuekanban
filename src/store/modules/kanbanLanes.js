const state = {
  lanes: [], // lanes seul
  nextLaneId: 1
};

const getters = {};

const mutations = {
  cleanLanes(state) {
    state.lanes.forEach(function (el, i) {
      delete el.items;
    });
  },
  deleteLanes(state) {
    state.lanes = [];
  },
  UPDATE_LANE_DRAG(state, items) {
    state.lanes = items;
  },
  UPDATE_LANE_ID: function (state) {
    state.nextLaneId += 1;
  },
  ADD_LANE: function (state, payload) {
    state.lanes.push(payload);
    payload.items = [];
  },
  UPDATE_LANE: function (state, {payload, index}) {
    state.lanes[index] = payload;
  },
  DELETE_LANE: function (state, payload) {
    state.lanes.splice(payload, 1);
  }
};

const actions = {

  UPDATE_LANE_DRAG_ACTION: function ({commit, state}, item) {
    commit('UPDATE_LANE_DRAG', item);
  },

  ADD_LANE_ACTION: function ({commit, state}, item) {
    let newItem = item;
    let newItemIndex = null;
    if (item.id && item.id > 0) {
      state.lanes.forEach(function (element, index) {
        if (element.id === item.id) {
          commit('UPDATE_LANE', item, index);
          newItemIndex = index;
        }
      });
    } else {
      newItem = Object.assign(item, {id: state.nextLaneId});
      commit('ADD_LANE', newItem);
      commit('UPDATE_LANE_ID');
    }


    this.dispatch('kanbanLanesItems/ADD_LANES_ITEMS_ACTION', {newItem, newItemIndex}, {root: true});
  },

  DELETE_LANE_ACTION: function ({commit, state}, item) {
    commit('DELETE_LANE', item);
  }
};

export default {
  namespaced: true,
  state,
  mutations,
  actions,
  getters
};