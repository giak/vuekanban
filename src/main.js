import '@babel/polyfill';
import Vue     from 'vue';

import './tools/plugins/axios';
import App     from './App.vue';
import router  from './router';
import {store} from './store/store';


import filters from "./tools/filters/filters";

filters.create(Vue);


// Form Select Multiple https://sagalbot.github.io/vue-select
import vSelect from 'vue-select';

Vue.component('v-select', vSelect);


import Vuelidate from 'vuelidate';

Vue.use(Vuelidate);

// Vue JSON Tree View (pour debug)
import TreeView from "vue-json-tree-view";

Vue.use(TreeView);

import VuePrism from 'vue-prism';

Vue.use(VuePrism);


import {library}         from '@fortawesome/fontawesome-svg-core';
import {
  faUser,
  faCopy,
  faCut,
  faEdit,
  faTrash,
  faImage,
  faFolder,
  faFolderOpen,
  faTimesCircle,
  faCircle,
  faBolt,
  faSave,
  faServer,
  faCheckSquare,
  faSquare,
  faGlobe
}                        from '@fortawesome/free-solid-svg-icons';
// import {faCircle}     from '@fortawesome/free-regular-svg-icons';
import {faVuejs}         from '@fortawesome/free-brands-svg-icons';
import {FontAwesomeIcon} from '@fortawesome/vue-fontawesome';

library.add(faUser,
  faCopy,
  faCut,
  faEdit,
  faTrash,
  faImage,
  faVuejs,
  faFolder,
  faFolderOpen,
  faTimesCircle,
  faCircle,
  faBolt,
  faSave,
  faServer,
  faCheckSquare,
  faSquare,
  faGlobe);

Vue.component('font-awesome-icon', FontAwesomeIcon);

import VModal from 'vue-js-modal';

Vue.use(VModal);

import VueLocalStorage from 'vue-localstorage';

Vue.use(VueLocalStorage);

import VueTextareaAutosize from 'vue-textarea-autosize';

Vue.use(VueTextareaAutosize);

import BootstrapVue from 'bootstrap-vue';

Vue.use(BootstrapVue);


import VueFormGenerator from "vue-form-generator";

Vue.use(VueFormGenerator);

Vue.config.productionTip = false;

// traductions
import {i18n} from './config/lang/lang';

new Vue({
  i18n,
  router,
  store,
  render: h => h(App)
}).$mount('#app');
