export const applyDrag = function (arr, dragResult) {
  const {removedIndex, addedIndex, payload} = dragResult;
  if (removedIndex === null && addedIndex === null) return arr;

  const result = arr;
  // const result = [...arr];
  let itemToAdd = payload;

  if (removedIndex !== null) {
    itemToAdd = result.splice(removedIndex, 1)[0];
  }

  if (addedIndex !== null) {
    result.splice(addedIndex, 0, itemToAdd);
  }

  return result;
};

export const generateItems = (count, creator) => {
  const result = [];
  for (let i = 0; i < count; i++) {
    result.push(creator(i));
  }
  return result;
};


export const applyDragLaneItem = function (arr, dragResult) {
  const {removedIndex, addedIndex, payload} = dragResult;
  console.log('removedIndex :: ', JSON.stringify(removedIndex, null, 2));
  console.log('addedIndex :: ', JSON.stringify(addedIndex, null, 2));
  console.log('payload :: ', JSON.stringify(payload, null, 2));


}

export const applyDragLane = function (arr, dragResult, idLane) {
  const {removedIndex, addedIndex, payload} = dragResult;

  if (removedIndex === null && addedIndex === null) {
    return arr;
  }

  const result = [...arr];
  let itemToAdd = payload;

  console.log('removedIndex :: ', JSON.stringify(removedIndex, null, 2));
  console.log('addedIndex :: ', JSON.stringify(addedIndex, null, 2));
  console.log('payload :: ', JSON.stringify(payload, null, 2));
  console.log('idLane :: ', JSON.stringify(idLane, null, 2));


  if (removedIndex === null && addedIndex !== null) {
    console.log('removedIndex = null - addedIndex >0 ::  itemToAdd:: ', JSON.stringify(itemToAdd, null, 2));
  }


  if (removedIndex !== null) {
    itemToAdd = result.splice(removedIndex, 1)[0];
    console.log('removedIndex itemToAdd :: ', JSON.stringify(itemToAdd, null, 2));
  }

  if (addedIndex !== null && itemToAdd) {
    result.splice(addedIndex, 0, itemToAdd);
    console.log('addedIndex itemToAdd :: ', JSON.stringify(itemToAdd, null, 2));
  }

};