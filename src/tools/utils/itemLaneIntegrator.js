/**
 * @param itemState l'item
 * @param nextId le prochain Id si nouveau (optionnel)
 */
export function setItem(itemState, nextId) {
  // faire une copie (autrement, impacte le state)
  let item = JSON.parse(JSON.stringify(itemState));
  if (!(item.lane && item.lane.id)) return null;
  let tmpIdLane = item.lane.id;

  if (!(item.id && item.id > 0)) {
    item = Object.assign(item, {id: nextId});
  }

  let tmpIdPriority = null;
  try {
    if (item.priority.id) {
      tmpIdPriority = item.priority.id;
    }
  } catch (e) {
  }

  let tmpIdProject = null;
  try {
    if (item.project.id) {
      tmpIdProject = item.project.id;
    }
  } catch (e) {
  }

  delete item.priority;
  delete item.project;
  delete item.lane;
  item.priority = tmpIdPriority;
  item.project = tmpIdProject;
  item.lane = tmpIdLane;

  return item;
}

/**
 * Recherche un Item avec son ID dans une liste et retourne son Index
 * @param items
 * @param id
 * @returns {*}
 */
export function getItemIndex(items, id) {
  for (const [index, el] of items.entries()) {
    if (el.id === id) {
      return index;
    }
  }
}

export function getObjectIndex(items, id) {
  items.forEach(function (el, index) {
    if (el.id === id) {
      return index;
    }
  });
}


/**
 *
 * @param parents
 * @param item
 * @returns {{parentIndex: *, itemIndex: *}}
 */
export function getParentItemIndex(parents, item) {
  let parentIndex = null;
  let parentItemIndex = null;

  parents.forEach(function (parentElement, parentElementIndex) {
    // console.log(' :: ', JSON.stringify(parentElement,null,2))
    if (parentElement.id === item.lane) {
      parentIndex = parentElementIndex;
      if (!parentElement.hasOwnProperty('items')) parentElement.items = [];
      if (parentElement.items.length > 0) {
        parentElement.items.forEach(function (element, index) {
          // console.log('element :: ', JSON.stringify(element, null, 2));
          if (element.id === item.id) {
            parentItemIndex = index;
          }
        });
      }
    }
  });

  return {"parentIndex": parentIndex, "parentItemIndex": parentItemIndex};
}


/**
 *
 * @param parents
 * @param item
 * @returns {*}
 */
export function setParentItemIndex(parents, item) {
  // console.log(' setParentItemIndex item :: ', JSON.stringify(item,null,2))

  let tmpParents = JSON.parse(JSON.stringify(parents));
  // console.log(' setParentItemIndex tmpParents :: ', JSON.stringify(tmpParents,null,2))

  let parentLanesItemIndex = getParentItemIndex(tmpParents, item);
  // console.log('parentLanesItemIndex :: ', JSON.stringify(parentLanesItemIndex,null,2))

  if (parentLanesItemIndex.parentItemIndex != null) {
    // console.log('update :: ', JSON.stringify(tmpParents[parentLanesItemIndex.parentIndex].items[parentLanesItemIndex.parentItemIndex], null, 2));
    tmpParents[parentLanesItemIndex.parentIndex].items[parentLanesItemIndex.parentItemIndex] = item;
  } else {
    // console.log('push :: ', JSON.stringify(item, null, 2));
    if (!tmpParents.hasOwnProperty('items')) tmpParents.items = [];
    tmpParents[parentLanesItemIndex.parentIndex].items.push(item);
  }

  // console.log('parents :: ', JSON.stringify(parents, null, 2));
  return tmpParents;


}
