export const multipleInputValidator = {
  currentMultipleInput: null,
  getMessage() {
    if (!this.currentMultipleInput.val1)
      return "Veuillez remplir le nom";
    if (!this.currentMultipleInput.val2)
      return "Veuillez remplir le code postal";
  },
  validate(multipleInput) {
    this.multipleInputValidator = multipleInput;

    if (!multipleInput.val1 || !multipleInput.val2) {
      return false;
    }

    return true;
  }
};