/**
 * permet d'étendre les directive ou {{}} moustache
 * Ex : {{ monnom | capitalized }}
 */

import dateFormat   from 'date-fns/format';
import dateLocaleFR from 'date-fns/locale/fr';

export default {
  filters: {
    capitalized: function (value) {
      if (!value) return '';
      value = value.toString();
      return value.charAt(0).toUpperCase() + value.slice(1);
    },
    highlightTerm: function (words, query) {
      let iQuery = new RegExp(query, "ig");
      return words.toString().replace(iQuery, function (matchedTxt) {
        return ('<span class=\'highlightTerm\'>' + matchedTxt + '</span>');
      });
    },
    truncateText: function (text, stop, clamp) {
      return text.slice(0, stop) + (stop < text.length ? clamp || '...' : '');
    },

    dateToFormat: function (date) {
      let humanDate = dateFormat(date, 'dddd DD MMMM YYYY', {locale: dateLocaleFR});
      if (humanDate === 'Invalid Date') { // 'Invalid Date' renvoyé par 'date-fns'
        return date;
      } else {
        return humanDate; // c'est une date
      }
    }
  },
  // référencement des "filters" dans Vue
  create: function (Vue) {
    Object.keys(this.filters).forEach(function (filter) {
      Vue.filter(filter, this.filters[filter]);
    }.bind(this));
  }
};