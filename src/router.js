import Vue       from 'vue';
import VueRouter from 'vue-router';

Vue.use(VueRouter);

const PageKanban = () => import(`./components/page/Kanban.vue`);
const backlog = () => import(`./components/modules/kanban/backlog/backlog.vue`);
const board = () => import(`./components/modules/kanban/board/board.vue`);
const lanes = () => import(`./components/modules/kanban/lanes/lanes.vue`);


export default new VueRouter({
  routes: [
    {
      name: `Kanban`,
      path: '/Kanban',
      component: PageKanban,
      id: 1,
      parent: 0
    },
    {
      name: `backlog`,
      path: '/backlog',
      component: backlog,
      id: 2,
      parent: 1
    },
    {
      name: `board`,
      path: '/board',
      component: board,
      id: 3,
      parent: 1
    },
    {
      name: `lanes`,
      path: '/lanes',
      component: lanes,
      id: 4,
      parent: 1
    }
    // mode: `history`
  ]
});
